.PHONY: help build tag push

TAG := $(shell git describe --always)
IMAGE := europeanspallationsource/dashboards-server
DASHBOARDS_SERVER_VER := 0.8.0

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build       to build the Docker image"
	@echo "  tag         to tag the Docker image"
	@echo "  push        to push the Docker image"

build:
	docker build --build-arg DASHBOARDS_SERVER_VER=${DASHBOARDS_SERVER_VER} -t $(IMAGE):latest .

tag: build
	docker tag $(IMAGE):latest $(IMAGE):$(TAG)

push: tag
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):latest
