Dockerfile to build dashboards-server ESS Docker image
======================================================

This Dockerfile creates a Docker_ image for the Jupyter Dashboards server.
It is based on https://github.com/jupyter-incubator/dashboards_setup/blob/master/docker_deploy/Dockerfile.dashboards.


Usage
-----

To update the jupyter-dashboards-server package, change the DASHBOARDS_SERVER_VER variable in the Makefile.
After updating this repository, you should tag it using the format "<jupyter-dashboards-server version>-ESSX"::

    $ git tag -a 0.8.0-ESS0

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ make push

This will create and push the image `europeanspallationsource/dashboards-server`.
The image is tagged with both `latest` and the `git tag`.


.. _Docker: https://www.docker.com
